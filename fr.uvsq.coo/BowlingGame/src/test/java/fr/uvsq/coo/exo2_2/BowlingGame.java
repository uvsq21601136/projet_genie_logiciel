package fr.uvsq.coo.exo2_2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BowlingGame {
	private Game g;
	@Before
	@Test public void initialiser() throws Exception {
		g = new Game ();
	}
	private void rollMany(int n, int pins) {
		for (int i = 0; i < n; i++)
		g.roll(pins);
		}
	@Test
	public void testGutterGame() throws Exception {
			rollMany(20, 0);
			int score=g.score();
			assertEquals(0,score);
		}
	@Test
		public void testAllOnes() throws Exception {
		rollMany(20,1);
		int score= g.score();
		assertEquals(20,score);
		}
	@Test
	 public void testOneSpare() throws Exception {
		rollSpare();
		g.roll(3);
		rollMany(17,0);
		int score=g.score();
		assertEquals(16,score);
		}
	@Test
	public void testOneStrike() throws Exception {
		rollStrike();
		g.roll(3);
		g.roll(4);
		rollMany(16, 0);
		assertEquals(24, g.score());
		}
	@Test
	public void testPerfectGame() throws Exception {
		rollMany(12,10);
		assertEquals(300, g.score());
		}
	private void rollStrike() {
		g.roll(10);
		}
	private void rollSpare() {
		g.roll(5);
		g.roll(5);
		}
}
